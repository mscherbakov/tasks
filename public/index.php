<?php

$yii = dirname(__FILE__) . '/../../yii/framework/yii.php';

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}

require_once($yii);

$config = require(dirname(__FILE__) . '/../protected/config/main.php');

$localConfigPath = dirname(__FILE__) . '/../protected/config/main-local.php';
if (file_exists($localConfigPath)) {
    $config = CMap::mergeArray($config, require($localConfigPath));
}

$debugConfigPath = dirname(__FILE__) . '/../protected/config/debug.php';
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' && file_exists($debugConfigPath)) {
    $config = CMap::mergeArray($config, require($debugConfigPath));
}

Yii::createWebApplication($config)->run();
