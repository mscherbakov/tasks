<?php

class TaskModule extends CWebModule
{
    public $defaultController = 'task';

    /**
     * Initializes the module.
     */
    public function init()
    {
        $this->setImport(array(
            'task.models.*',
        ));

        Yii::app()->getModule('user');
    }
}
