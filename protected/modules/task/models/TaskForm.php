<?php

class TaskForm extends CFormModel
{
    public $name;

    private $_model;

    public function __construct(Task $model)
    {
        parent::__construct($model->getScenario());

        $this->_model = $model;
        $this->name = $model->name;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 255),
        );
    }

    /**
     * Saves the record of current form.
     *
     * @return boolean whether the saving succeeds
     */
    public function save()
    {
        if ($this->validate()) {
            $this->_model->name = $this->name;
            if ($this->_model->getIsNewRecord()) {
                $this->_model->user_id = Yii::app()->getUser()->getId();
                $this->_model->order = Task::model()->byUser($this->_model->user_id)->getMaxOrder() + 1;
            }

            return $this->_model->save();
        }

        return false;
    }
}
