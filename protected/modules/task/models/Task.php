<?php

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $order
 * @property string $name
 *
 * @property User $user
 */
class Task extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Task the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{task}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, order, name', 'required'),
            array('user_id', 'exist', 'className' => 'User', 'attributeName' => 'id'),
            array('order', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * Returns the default named scope that should be implicitly applied to all queries for this model.
     *
     * @return array the query criteria. This will be used as the parameter to the constructor
     */
    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);

        return array(
            'order' => $alias . '.order',
        );
    }

    /**
     * This method is invoked after saving a record successfully.
     */
    protected function afterSave()
    {
        $this->user->refreshTasksOrder();

        parent::afterSave();
    }

    /**
     * Query result would be indexed by specified column
     *
     * @param string $column
     * @return $this
     */
    public function indexBy($column)
    {
        $this->getDbCriteria()->index = $column;

        return $this;
    }

    /**
     * Specified user would be applied to criteria
     *
     * @param integer $user
     * @return $this
     */
    public function byUser($user)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.user_id', $user);

        return $this;
    }

    /**
     * Returns maximum order column value for query result
     *
     * @return integer
     */
    public function getMaxOrder()
    {
        $query = $this->getDbConnection()->createCommand()->
            select('max(`order`) as max')->
            from($this->tableName() . ' ' . $this->getTableAlias())->
            where($this->getDbCriteria()->condition, $this->getDbCriteria()->params);

        $result = $query->queryScalar();
        if ($result === null) {
            $result = 0;
        }

        return $result;
    }

    /**
     * Specified id would be applied to criteria
     *
     * @param integer|integer[] $id
     * @return $this
     */
    public function byId($id)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.id', $id);

        return $this;
    }

    /**
     * Returns option list data for query result
     *
     * @return array
     */
    public function getOptionList()
    {
        return CHtml::listData($this->findAll(), 'id', 'name');
    }
}
