<?php

/**
 * @property integer $id
 * @property string $object_type
 * @property integer $object_id
 * @property integer $number
 * @property string $name
 * @property string $old_value
 * @property string $new_value
 *
 * @property Revision[] $revision
 */
class RevisionAttribute extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RevisionAttribute the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{revision_attribute}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('object_type, object_id, number, name', 'required'),
            array('object_id, number', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'revision' => array(
                self::BELONGS_TO,
                'Revision',
                array(
                    'object_type' => 'object_type',
                    'object_id' => 'object_type',
                    'number' => 'number',
                )
            ),
        );
    }
}
