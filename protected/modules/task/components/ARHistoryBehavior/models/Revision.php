<?php

/**
 * @property string $object_type
 * @property integer $object_id
 * @property integer $number
 * @property string $type
 * @property integer $create_time
 *
 * @property RevisionAttribute[] $attributes
 */
class Revision extends CActiveRecord
{
    const TYPE_INSERT = 'insert';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETE = 'delete';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Revision the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{revision}}';
    }

    /**
     * Returns a list of behaviors that this model should behave as.
     *
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null,
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('object_type, object_id, type', 'required'),
            array('object_id, number', 'numerical', 'integerOnly' => true),
            array('type', 'in', 'range' => array_keys(self::getTypeOptions())),
            array('number', 'default', 'value' => 0),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'attributes' => array(
                self::HAS_MANY,
                'RevisionAttribute',
                array(
                    'object_type' => 'object_type',
                    'object_id' => 'object_id',
                    'number' => 'number',
                )
            ),
        );
    }


    /**
     * Returns type options
     *
     * @return array
     */
    public static function getTypeOptions()
    {
        return array(
            self::TYPE_INSERT => 'Insert',
            self::TYPE_UPDATE => 'Update',
            self::TYPE_DELETE => 'Delete',
        );
    }

    /**
     * Returns new attribute model for current revision
     *
     * @return RevisionAttribute
     * @throws Exception
     */
    public function createAttribute()
    {
        if ($this->getIsNewRecord()) {
            throw new Exception('Could not create attribute for non saved revision');
        }

        $result = new RevisionAttribute();
        $result->object_type = $this->object_type;
        $result->object_id = $this->object_id;
        $result->number = $this->number;

        return $result;
    }

    /**
     * Specified object type would be applied to criteria
     *
     * @param string $objectType
     * @return $this
     */
    public function byObjectType($objectType)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.object_type', $objectType);

        return $this;
    }

    /**
     * Specified object id would be applied to criteria
     *
     * @param integer $objectId
     * @return $this
     */
    public function byObjectId($objectId)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.object_id', $objectId);

        return $this;
    }

    /**
     * Query would be sorted by specified column
     *
     * @param string $column
     * @param bool $isDesc whether to sort by descending
     * @return $this
     */
    public function sortBy($column, $isDesc = false)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->order = $alias . '.' . $column . ($isDesc ? ' DESC' : ' ASC');

        return $this;
    }
}
