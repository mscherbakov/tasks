<?php

class ARHistoryBehavior extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $objectType;

    /**
     * @var array
     */
    public $allowedAttributes;

    private $_oldValues;
    private $_revisions;

    /**
     * @var CDbTransaction
     */
    private $_transaction;

    private $_revisionTable = '{{revision}}';
    private $_revisionAttributeTable = '{{revision_attribute}}';

    /**
     * Attaches the behavior object to the component.
     *
     * @param CComponent $owner the component that this behavior is to be attached to.
     * @throws Exception if objectType is not specified
     */
    public function attach($owner)
    {
        if (empty($this->objectType) || !is_string($this->objectType)) {
            throw new Exception('objectType field should be specified');
        }

        $db = Yii::app()->getDb();
        if ($db->schema->getTable($this->_revisionTable) === null) {
            $this->createRevisionTable();
        }
        if ($db->schema->getTable($this->_revisionAttributeTable) === null) {
            $this->createRevisionAttributeTable();
        }

        $modelsPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'models';
        require_once($modelsPath . DIRECTORY_SEPARATOR . 'Revision.php');
        require_once($modelsPath . DIRECTORY_SEPARATOR . 'RevisionAttribute.php');

        parent::attach($owner);
    }

    /**
     * @return Revision[]
     */
    public function getRevisions()
    {
        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();

        if ($this->_revisions === null) {
            $this->_revisions = array();

            /**
             * @var Revision[] $revisions
             */
            $revisions = Revision::model()->byObjectType($this->objectType)->byObjectId($owner->getPrimaryKey())->findAll();
            foreach ($revisions as $revision) {
                $this->_revisions[$revision->number] = $revision;
            }
        }

        return $this->_revisions;
    }

    /**
     * Responds to {@link CActiveRecord::onAfterFind} event.
     *
     * @param CEvent $event event parameter
     */
    public function afterFind($event)
    {
        $this->_oldValues = $this->getAllowedValues();
    }

    /**
     * Creates revision table
     */
    private function createRevisionTable()
    {
        Yii::app()->getDb()->createCommand()->createTable(
            $this->_revisionTable,
            array(
                'object_type' => 'string NOT NULL',
                'object_id' => 'integer NOT NULL',
                'number' => 'integer NOT NULL',
                'type' => 'ENUM("insert","update","delete")',
                'create_time' => 'integer NOT NULL',
                'primary key (`object_type`,`object_id`,`number`)',
            )
        );
        Yii::app()->getDb()->createCommand('
CREATE TRIGGER revision_before_insert
	BEFORE INSERT
	ON ' . $this->_revisionTable . '
	FOR EACH ROW
BEGIN
  SET new.number = IFNULL((SELECT max(number) FROM ' . $this->_revisionTable . ' r where r.object_type = new.object_type AND r.object_id = new.object_id), 0) + 1;
END')->execute();
    }

    /**
     * Creates revision attribute table
     */
    private function createRevisionAttributeTable()
    {
        $dbCommand = Yii::app()->getDb()->createCommand();
        $dbCommand->createTable(
            $this->_revisionAttributeTable,
            array(
                'id' => 'pk',
                'object_type' => 'string NOT NULL',
                'object_id' => 'integer NOT NULL',
                'number' => 'integer NOT NULL',
                'name' => 'string NOT NULL',
                'old_value' => 'text',
                'new_value' => 'text',
            )
        );
        $dbCommand->addForeignKey(
            'revisionAttributeFk',
            $this->_revisionAttributeTable,
            'object_type, object_id, number',
            $this->_revisionTable,
            'object_type, object_id, number'
        );
    }

    /**
     * If transaction does not exist, begins transactions
     */
    private function beginTransaction()
    {
        if (!Yii::app()->getDb()->getCurrentTransaction()) {
            $this->_transaction = Yii::app()->getDb()->beginTransaction();
        }
    }

    /**
     * If transaction was began by this behavior, it would be committed
     */
    private function commitTransaction()
    {
        if ($this->_transaction) {
            $this->_transaction->commit();
            $this->_transaction = null;
        }
    }

    /**
     * Responds to {@link CActiveRecord::onBeforeSave} event.
     *
     * @param CModelEvent $event event parameter
     */
    public function beforeSave($event)
    {
        $this->beginTransaction();
    }

    /**
     * Responds to {@link CActiveRecord::onAfterSave} event.
     *
     * @param CModelEvent $event event parameter
     */
    public function afterSave($event)
    {
        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();

        $changedAttributes = array();
        $newValues = $this->getAllowedValues();
        foreach ($newValues as $attributeName => $attributeValue) {
            if (!isset($this->_oldValues[$attributeName]) || $this->_oldValues[$attributeName] !== $attributeValue) {
                $changedAttributes[$attributeName] = $attributeValue;
            }
        }

        if (!empty($changedAttributes)) {
            $revision = $this->createNewRevision($owner->getIsNewRecord() ? Revision::TYPE_INSERT : Revision::TYPE_UPDATE);

            foreach ($changedAttributes as $attributeName => $attributeValue) {
                $attribute = $revision->createAttribute();
                $attribute->name = $attributeName;
                $attribute->old_value = isset($this->_oldValues[$attributeName]) ? $this->_oldValues[$attributeName] : null;
                $attribute->new_value = $attributeValue;
                $attribute->save();
            }
        }

        $this->commitTransaction();

        $this->_oldValues = $newValues;
    }

    /**
     * Responds to {@link CActiveRecord::onBeforeDelete} event.
     *
     * @param CEvent $event event parameter
     */
    public function beforeDelete($event)
    {
        $this->beginTransaction();
    }

    /**
     * Responds to {@link CActiveRecord::onBeforeDelete} event.
     *
     * @param CEvent $event event parameter
     */
    public function afterDelete($event)
    {
        if (!empty($this->_oldValues)) {
            $revision = $this->createNewRevision(Revision::TYPE_DELETE);

            foreach ($this->_oldValues as $attributeName => $attributeValue) {
                $attribute = $revision->createAttribute();
                $attribute->name = $attributeName;
                $attribute->old_value = $attributeValue;
                $attribute->new_value = null;
                $attribute->save();
            }

            $this->_oldValues = array();
        }

        $this->commitTransaction();
    }

    /**
     * @param string $type
     * @return Revision
     */
    private function createNewRevision($type)
    {
        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();

        $result = new Revision();
        $result->object_type = $this->objectType;
        $result->object_id = $owner->getPrimaryKey();
        $result->type = $type;
        $result->save();

        //reload model, cause of trigger assignment to `number` field
        $result = Revision::model()->
            byObjectType($this->objectType)->
            byObjectId($owner->getPrimaryKey())->
            sortBy('number', true)->find();

        return $result;
    }

    /**
     * Returns values for allowed attributes
     *
     * @return array
     */
    public function getAllowedValues()
    {
        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();

        return $owner->getAttributes(empty($this->allowedAttributes) ? true : $this->allowedAttributes);
    }

    /**
     * Recovers models's attribute to specified revision number (without saving)
     *
     * @param integer $number
     * @return CActiveRecord
     */
    public function getModelByRevisionNumber($number)
    {
        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();

        if (isset($owner->revisions[$number])) {
            /**
             * @var Revision $revision
             */
            $revision = $owner->revisions[$number];

            $result = clone $owner;
            foreach ($revision->getRelated('attributes') as $attribute) {
                $result[$attribute->name] = $attribute->new_value;
            }

            return $result;
        }

        return null;
    }
}
