<?php

/**
 * @var TaskController $this
 * @var integer $revisionNumber
 * @var array $revisionOptions
 * @var array $taskOptions
 */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'History' => array('history'),
    'Revision #' . $revisionNumber => array('history', 'number' => $revisionNumber),
);
$this->menu = array(
    array(
        'label' => 'Task List',
        'url' => array('index'),
    ),
    array(
        'label' => 'Create Task',
        'url' => array('create'),
    ),
);
?>
    <h1>Tasks History</h1>
    <ul>
        <?php foreach ($revisionOptions as $revisionNumber => $revisionName): ?>
            <li><?php echo CHtml::link($revisionName, array('history', 'number' => $revisionNumber)); ?></li>
        <?php endforeach; ?>
    </ul>
    <h2>Tasks snapshot</h2>
<?php
$this->widget(
    'zii.widgets.jui.CJuiSortable',
    array(
        'items' => $taskOptions,
        'options' => array(
            'disabled' => true,
        ),
    )
);
