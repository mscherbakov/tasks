<?php
/**
 * @var CForm $this
 */

return array(
    'title' => '',
    'showErrorSummary' => true,
    'elements' => array(
        'name' => array(
            'maxlength' => 255,
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'label' => 'Save',
        ),
    ),
);
