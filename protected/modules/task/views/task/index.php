<?php

/**
 * @var TaskController $this
 * @var array $taskOptions
 */

$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'Manage'
);
$this->menu = array(
    array(
        'label' => 'Task List',
        'url' => array('index'),
    ),
    array(
        'label' => 'Tasks History',
        'url' => array('history'),
    ),
    array(
        'label' => 'Create Task',
        'url' => array('create'),
    ),
);
?>
<h1>Manage Tasks</h1>
<?php
$this->widget(
    'zii.widgets.jui.CJuiSortable',
    array(
        'items' => $taskOptions,
        'options' => array(
            'update' => new CJavaScriptExpression('function(){$.post("'.$this->createUrl('reorder').'", {tasks:$(this).sortable("toArray")})}'),
        ),
    )
);
