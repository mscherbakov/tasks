<?php
/**
 * @var TaskController $this
 * @var CForm $form
 */
$this->breadcrumbs = array(
    'Tasks' => array('index'),
    'Create',
);
$this->menu = array(
    array('label' => 'Task List', 'url' => array('index')),
    array('label' => 'Tasks History', 'url' => array('history')),
);
?>
<h1>Create Task</h1>
<?php echo $form ?>
