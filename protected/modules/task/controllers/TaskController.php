<?php

class TaskController extends Controller
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array(
                    'allow',
                    'users' => array('@'),
                ),
            ),
            parent::accessRules()
        );
    }

    /**
     * Renders task list
     */
    public function actionIndex()
    {
        $taskOptions = Task::model()->byUser(Yii::app()->getUser()->getId())->getOptionList();

        $this->render('index', array('taskOptions' => $taskOptions));
    }

    /**
     * Creates model
     */
    public function actionCreate()
    {
        Yii::import('bootstrap.widgets.TbForm');
        /**
         * @var CForm $form
         */
        $form = TbForm::createForm('task.views.task.form', $this, array(), new TaskForm(new Task()));
        if ($form->submitted() && $form->getModel()->save()) {
            $this->redirect(array('index'));
        }

        $this->render('create', array('form' => $form));
    }

    /**
     * Performs user's tasks reordering
     */
    public function actionReorder()
    {
        $taskOrders = Yii::app()->getRequest()->getPost('tasks');
        if (is_array($taskOrders)) {
            /**
             * @var Task[] $tasks
             */
            $tasks = Task::model()->byUser(Yii::app()->getUser()->getId())->indexBy('id')->findAll();
            $taskOrders = array_intersect($taskOrders, array_keys($tasks));

            $order = 1;
            $transaction = Yii::app()->getDb()->beginTransaction();

            //perform reordering for specified tasks
            foreach ($taskOrders as $taskId) {
                $tasks[$taskId]->order = $order++;
                $tasks[$taskId]->save();
            }

            //perform reordering for non specified tasks
            foreach ($tasks as $task) {
                if (!in_array($task->id, $taskOrders)) {
                    $task->order = $order++;
                    $task->save();
                }
            }

            $transaction->commit();
        }

        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            $this->redirect(array('index'));
        }
    }

    /**
     * Renders tasks history page
     *
     * @param integer $number
     * @throws CHttpException if revision number does not exist
     */
    public function actionHistory($number = null)
    {
        /**
         * @var User $user
         */
        $user = User::model()->findByPk(Yii::app()->getUser()->getId());
        if (empty($number)) {
            $number = max(array_keys($user->revisions));
        }

        /**
         * @var User $revisionUser
         */
        if ((!$revisionUser = $user->getModelByRevisionNumber($number))) {
            throw new CHttpException(404);
        }

        $sortedTaskOptions = array();

        $tasksOrder = unserialize($revisionUser->tasks_order);
        if (is_array($tasksOrder) && !empty($tasksOrder)) {
            /**
             * @var Task[] $tasks;
             */
            $tasks = Task::model()->byId($tasksOrder)->indexBy('id')->findAll();
            foreach ($tasksOrder as $taskId) {
                $sortedTaskOptions[$taskId] = $tasks[$taskId]->name;
            }
        }

        $revisionOptions = array();
        foreach ($user->revisions as $revision) {
            /**
             * @var Revision $revision
             */
            $revisionOptions[$revision->number] = '# ' . $revision->number . ' (' . Yii::app()->format->formatDatetime($revision->create_time) . ')';
        }

        $this->render(
            'history',
            array(
                'revisionNumber' => $number,
                'revisionOptions' => $revisionOptions,
                'taskOptions' => $sortedTaskOptions
            )
        );
    }
}
