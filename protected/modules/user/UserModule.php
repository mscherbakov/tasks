<?php

class UserModule extends CWebModule
{
    /**
     * @var string the ID of the default controller for this module.
     */
    public $defaultController = 'user';

    /**
     * Initializes the module.
     */
    public function init()
    {
        $this->setImport(
            array(
                'user.components.*',
                'user.models.*',
            )
        );
    }

    /**
     * @return string
     */
    public function getLogoutUrl()
    {
        return Yii::app()->createUrl('user/login/logout');
    }
}
