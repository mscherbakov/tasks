<?php
/**
 * @var CForm $this
 */

return array(
    'title' => '',
    'showErrorSummary' => true,
    'elements' => array(
        'username' => array(
            'maxlength' => 255,
        ),
        'password' => array(
            'type' => 'password',
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'label' => 'Login',
        ),
    ),
);
