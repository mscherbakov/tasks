<?php

/**
 * @var UserController $this
 * @var CForm $form
 */
$model = $form->getModel();

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);
$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
);
?>
<h1>Update User <?php echo $form->getModel()->id; ?></h1>
<?php echo $form; ?>
