<?php
/**
 * @var UserController $this
 * @var CForm $form
 */
$this->breadcrumbs = array(
    'Users' => array('index'),
    'Create',
);
$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
);
?>
<h1>Create User</h1>
<?php echo $form ?>
