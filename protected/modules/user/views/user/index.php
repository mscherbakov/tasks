<?php

/**
 * @var DefaultController $this
 */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage'
);
$this->menu = array(
    array(
        'label' => 'List User',
        'url' => array('index'),
    ),
    array(
        'label' => 'Create User',
        'url' => array('create'),
    ),
);
?>
    <h1>Manage Users</h1>
<?php
$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'id' => 'user-grid',
        'dataProvider' => new CActiveDataProvider('User'),
        'columns' => array(
            'id',
            'username',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}',
            ),
        ),
    )
);
