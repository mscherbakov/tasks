<?php

class LoginController extends Controller
{
    public $defaultAction = 'login';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array(
                    'allow',
                    'actions' => array('login', 'logout'),
                    'users' => array('*'),
                ),
            ),
            parent::accessRules()
        );
    }

    /**
     * Performs user login
     */
    public function actionLogin()
    {
        Yii::import('bootstrap.widgets.TbForm');

        $form = TbForm::createForm('user.views.login.form', $this, array(), new LoginForm());
        if ($form->submitted() && $form->getModel()->login()) {
            $this->redirect(Yii::app()->getUser()->returnUrl);
        }

        $this->render('index', array('form' => $form));
    }

    /**
     * Performs user logout
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();

        $this->redirect(Yii::app()->getHomeUrl());
    }
}
