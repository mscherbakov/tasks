<?php

class UserController extends Controller
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return CMap::mergeArray(
            array(
                array(
                    'allow',
                    'actions' => array('index', 'create', 'update'),
                    'roles' => array('administrator'),
                ),
            ),
            parent::accessRules()
        );
    }

    /**
     * Lists all users
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Performs editing and saving specified UserForm
     *
     * @param UserForm $model
     */
    private function edit(UserForm $model)
    {
        Yii::import('bootstrap.widgets.TbForm');
        /**
         * @var CForm $form
         */
        $form = TbForm::createForm('user.views.user.form', $this, array(), $model);
        if ($form->submitted() && $form->getModel()->save()) {
            $this->redirect(array('index'));
        }

        $this->render(
            $model->getIsNewRecord() ? 'create' : 'update',
            array('form' => $form)
        );
    }

    /**
     * Creates model
     */
    public function actionCreate()
    {
        $model = new UserForm(new User());

        $this->edit($model);
    }

    /**
     * Updates a particular model.
     *
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = new UserForm($this->loadModel($id));

        $this->edit($model);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     *
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }
}
