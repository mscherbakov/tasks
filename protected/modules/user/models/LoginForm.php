<?php

class LoginForm extends CFormModel
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('username, password', 'safe'),
        );
    }

    /**
     * Returns login operation result
     *
     * @return bool whether user was logged successfully
     */
    public function login()
    {
        $userIdentity = new UserIdentity($this->username, $this->password);
        if ($userIdentity->authenticate()) {
            $duration = 3600 * 24 * 30;
            Yii::app()->getUser()->login($userIdentity, $duration);
            return true;
        } else {
            $this->addError('password', 'Wrong username/password pair');
            return false;
        }
    }
}
