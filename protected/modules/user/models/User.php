<?php

/**
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $tasks_order
 *
 * @property Task[] $tasks
 * @property Revision[] $revisions
 */
class User extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * Returns a list of behaviors that this model should behave as.
     *
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array(
            'ARHistoryBehavior' => array(
                'class' => 'task.components.ARHistoryBehavior.ARHistoryBehavior',
                'objectType' => 'user',
                'allowedAttributes' => array('tasks_order'),
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('username', 'required'),
            array('username', 'length', 'max' => 255),
            array('password_hash', 'length', 'max' => 63),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'tasks' => array(self::HAS_MANY, 'Task', 'user_id', 'index' => 'id'),
        );
    }

    /**
     * Refreshes tasks_order field
     *
     * @return bool whether refreshing processed successfully
     */
    public function refreshTasksOrder()
    {
        $tasksOrder = array_keys($this->getRelated('tasks', true));
        $this->tasks_order = serialize($tasksOrder);

        return $this->save();
    }

    /**
     * Sets new password
     *
     * @param string $password new password
     */
    public function setPassword($password)
    {
        $this->password_hash = crypt($password);
    }

    /**
     * Performs password validation
     *
     * @param string $password
     *
     * @return bool whether password validated
     */
    public function validatePassword($password)
    {
        return $this->password_hash === crypt($password, $this->password_hash);
    }

    /**
     * Specified username would be applied to criteria
     *
     * @param string $username
     * @return $this
     */
    public function byUsername($username)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.username', $username);

        return $this;
    }
}
