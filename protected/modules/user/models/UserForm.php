<?php

class UserForm extends CFormModel
{
    public $username;
    public $password;
    public $passwordConfirm;

    private $_model;

    public function __construct(User $model)
    {
        parent::__construct($model->getScenario());

        $this->_model = $model;
        $this->username = $model->username;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $result = array(
            array('username', 'required'),
            array('username', 'length', 'max' => 255),
            array(
                'username',
                'UniqueAttributeValidator',
                'className' => 'User',
                'ignoreValue' => $this->_model->username,
                'message' => 'User with specified {attribute} already exist.'
            ),
            array('passwordConfirm', 'compare', 'compareAttribute' => 'password'),
        );

        if ($this->getIsNewRecord()) {
            $result[] = array('password, passwordConfirm', 'required');
        } else {
            $result[] = array('password', 'safe');
        }

        return $result;
    }

    /**
     * Returns the attribute labels.
     *
     * @return array attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $result = array('passwordConfirm' => 'Confirm password');

        if (!$this->getIsNewRecord()) {
            $result['password'] = 'New password';
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_model->id;
    }

    /**
     * @return boolean whether current user is new
     */
    public function getIsNewRecord()
    {
        return $this->_model->getIsNewRecord();
    }

    /**
     * Saves the record of current form.
     *
     * @return boolean whether the saving succeeds
     */
    public function save()
    {
        if ($this->validate()) {
            $this->_model->username = $this->username;
            if (!empty($this->password)) {
                $this->_model->setPassword($this->password);
            }

            return $this->_model->save();
        }

        return false;
    }
}
