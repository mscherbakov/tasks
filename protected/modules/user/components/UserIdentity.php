<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
     * Authenticates a user based on {@link username} and {@link password}.
     *
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        /**
         * @var User $user
         */
        $user = User::model()->byUsername($this->username)->find();
        if (!$user) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (!$user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->id;
            $this->username = $user->username;
            $this->errorCode = self::ERROR_NONE;
        }

        return $this->errorCode == self::ERROR_NONE;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->_id;
    }
}
