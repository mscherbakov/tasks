<?php

class UniqueAttributeValidator extends CValidator
{
    public $message = 'Record with specified {attribute} already exists.';
    public $className;

    /**
     * @var string|string[] if validated attribute is equal to (or equal to one of) this param, it would not be validated
     */
    public $ignoreValue;

    /**
     * Validates a single attribute.
     *
     * @param CActiveRecord $object the data object being validated
     * @param string $attribute the name of the attribute to be validated.
     */
    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;
        if (empty($value)) {
            return;
        }

        if (!empty($this->ignoreValue)) {
            if (is_string($this->ignoreValue) && $this->ignoreValue == $value) {
                return;
            }
            if (is_array($this->ignoreValue) && in_array($value, $this->ignoreValue)) {
                return;
            }
        }

        $className = $this->className;
        if (empty($className)) {
            $className = get_class($object);
        }

        /**
         * @var CActiveRecord $finder
         */
        $finder = new $className();

        $criteria = new CDbCriteria();
        $criteria->compare($attribute, $value);

        if ($finder->exists($criteria)) {
            $this->addError($object, $attribute, $this->message);
        }
    }
}
