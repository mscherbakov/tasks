<?php

class ErrorController extends Controller
{
    public $defaultAction = 'error';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('*'),
            ),
        );
    }

    /**
     * Renders error info page
     *
     * @throws CHttpException
     */
    public function actionError()
    {
        if (!($error = Yii::app()->errorHandler->error)) {
            throw new CHttpException(404);
        }

        $this->render('error', $error);
    }
}
