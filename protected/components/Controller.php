<?php

/**
 * Base backend controller
 */
abstract class Controller extends CController
{
    public $breadcrumbs = array();
    public $menu = array();

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     *
     * @param CAction $action the action to be executed.
     *
     * @return boolean whether the action should be executed.
     */
    protected function beforeAction($action)
    {
        if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
            /**
             * @var CAssetManager $assetManager
             */
            $assetManager = Yii::app()->getAssetManager();
            $assetsPath = $assetManager->publish(Yii::getPathOfAlias('application.assets'));

            /**
             * @var CClientScript $clientScript
             */
            $clientScript = Yii::app()->getClientScript();
            $clientScript->registerCssFile($assetsPath . '/style.css');
        }

        return parent::beforeAction($action);
    }


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }
}
