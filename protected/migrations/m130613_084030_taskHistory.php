<?php

class m130613_084030_taskHistory extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{user}}', 'tasks_order', 'text');
    }

    public function down()
    {
        $this->dropColumn('{{user}}', 'tasks_order');
    }
}
