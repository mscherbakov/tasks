<?php

class m130613_072940_tasks extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            '{{task}}',
            array(
                'id' => 'pk',
                'user_id' => 'integer NOT NULL',
                'order' => 'integer NOT NULL',
                'name' => 'string NOT NULL',
            )
        );
        $this->addForeignKey('taskUser', '{{task}}', 'user_id', '{{task}}', 'id');
        $this->createIndex('taskUserOrder', '{{task}}', 'user_id, order');
    }

    public function down()
    {
        $this->dropForeignKey('taskUser', '{{task}}');
        $this->dropTable('{{task}}');
    }
}
