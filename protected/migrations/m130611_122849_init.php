<?php

class m130611_122849_init extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            '{{user}}',
            array(
                'id' => 'pk',
                'username' => 'string NOT NULL',
                'password_hash' => 'VARCHAR(63)',
            )
        );
        $this->createIndex('username', '{{user}}', 'username', true);

        Yii::app()->getModule('user');

        $user = new User();
        $user->username = 'admin';
        $user->setPassword('admin');
        $user->save();

        $this->createTable(
            '{{auth_item}}',
            array(
                'name' => 'VARCHAR(64) NOT NULL',
                'type' => 'integer NOT NULL',
                'description' => 'text',
                'bizrule' => 'text',
                'data' => 'text',
                'primary key (`name`)',
            )
        );

        $this->createTable(
            '{{auth_item_child}}',
            array(
                'parent' => 'VARCHAR(64) NOT NULL',
                'child' => 'VARCHAR(64) NOT NULL',
                'primary key (`parent`, `child`)',
            )
        );
        $this->addForeignKey('authItemParent', '{{auth_item_child}}', 'parent', '{{auth_item}}', 'name');
        $this->addForeignKey('authItemChild', '{{auth_item_child}}', 'child', '{{auth_item}}', 'name');

        $this->createTable(
            '{{auth_assignment}}',
            array(
                'itemname' => 'VARCHAR(64) NOT NULL',
                'userid' => 'VARCHAR(64) NOT NULL',
                'bizrule' => 'text',
                'data' => 'text',
                'primary key (`itemname`, `userid`)',
            )
        );
        $this->addForeignKey('authAssignmentItemname', '{{auth_assignment}}', 'itemname', '{{auth_item}}', 'name');

        /**
         * @var CDbAuthManager $auth
         */
        $auth = Yii::app()->getComponent('authManager');
        $auth->createRole('administrator', 'Administrators');
        $auth->assign('administrator', $user->id);
    }

    public function down()
    {
        $this->dropForeignKey('authAssignmentItemname', '{{auth_assignment}}');
        $this->dropTable('{{auth_assignment}}');

        $this->dropForeignKey('authItemChild', '{{auth_item_child}}');
        $this->dropForeignKey('authItemParent', '{{auth_item_child}}');
        $this->dropTable('{{auth_item_child}}');

        $this->dropTable('{{auth_item}}');

        $this->dropTable('{{user}}');
    }
}