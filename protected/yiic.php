<?php

$yiic = dirname(__FILE__) . '/../../yii/framework/yiic.php';
$config = require(dirname(__FILE__) . '/config/console.php');

$localConfigPath = dirname(__FILE__) . '/config/console-local.php';
if (file_exists($localConfigPath)) {
    $config = CMap::mergeArray($config, require($localConfigPath));
}

require_once($yiic);
