<?php

Yii::import('bootstrap.widgets.TbNavbar');
class Navbar extends TbNavbar
{
    /**
     * Initializes the widget.
     */
    public function init()
    {
        $this->items = array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'items' => array(
                    array(
                        'label' => 'Tasks',
                        'url' => array('/task'),
                        'visible' => !Yii::app()->getUser()->getIsGuest(),
                    ),
                    array(
                        'label' => 'Users',
                        'url' => array('/user'),
                        'visible' => Yii::app()->getUser()->checkAccess('administrator'),
                    ),
                )
            ),
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => array('class' => 'pull-right'),
                'items' => array(
                    array(
                        'label' => 'Войти',
                        'url' => Yii::app()->getUser()->loginUrl,
                        'visible' => Yii::app()->getUser()->getIsGuest(),
                    ),
                    array(
                        'label' => 'Выйти (' . Yii::app()->getUser()->getName() . ')',
                        'url' => Yii::app()->getModule('user')->getLogoutUrl(),
                        'visible' => !Yii::app()->getUser()->getIsGuest(),
                    ),
                )
            )
        );

        parent::init();
    }
}
