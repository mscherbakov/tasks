<?php
/**
 * @var Controller $this
 * @var string $content
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?php echo $this->getPageTitle(); ?></title>
</head>
<body>
<?php $this->widget('application.widgets.Navbar'); ?>
<div class="container">
    <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array('links' => $this->breadcrumbs)); ?>
    <?php $this->widget('bootstrap.widgets.TbMenu', array('type' => 'list', 'items' => $this->menu)); ?>
    <?php echo $content; ?>
</div>
</body>
</html>