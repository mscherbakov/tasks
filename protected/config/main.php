<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Tasks',
    'defaultController' => 'task',
    'preload' => array(
        'bootstrap',
    ),
    'import' => array(
        'application.components.*',
    ),
    'modules' => array(
        'user',
        'task',
    ),
    'components' => array(
        'bootstrap' => array(
            'class' => 'ext.bootstrap.src.components.Bootstrap',
        ),
        'db' => require(dirname(__FILE__) . '/db.php'),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'itemTable' => '{{auth_item}}',
            'itemChildTable' => '{{auth_item_child}}',
            'assignmentTable' => '{{auth_assignment}}',
        ),
        'errorHandler' => array(
            'errorAction' => 'error',
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'loginUrl' => array('/user/login'),
        ),
        'cache' => array(
            'class' => 'system.caching.CDummyCache',
        ),
    ),
);
