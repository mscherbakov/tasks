<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'modules' => array(
        'user',
        'task',
    ),
    'components' => array(
        'db' => require(dirname(__FILE__) . '/db.php'),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'itemTable' => '{{auth_item}}',
            'itemChildTable' => '{{auth_item_child}}',
            'assignmentTable' => '{{auth_assignment}}',
        ),
    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationTable' => '{{migration}}',
        ),
    ),
);
