<?php

$result = array(
    'connectionString' => 'mysql:host=localhost;dbname=tasks',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'tablePrefix' => '',
    'schemaCachingDuration' => 3600,
);

$localConfigPath = dirname(__FILE__) . '/db-local.php';
if (file_exists($localConfigPath)) {
    $result = array_merge($result, require($localConfigPath));
}

return $result;
